# Ejecutar con bin/descarga_indice_imagenes
require "json"
def descarga
  max_id_externo = Fotoe14.where(eleccion_id: Rails.configuration.x.eleccion)
                          .order(id_externo: :desc)&.first&.id_externo || 0

  api_url = Rails.configuration.x.api_fotoe14 % [max_id_externo, Rails.configuration.x.tamano_pagina]
    
  if File.exist?('/tmp/i.json')
    File.unlink('/tmp/i.json')
  end
  comando = "curl \"#{api_url}\" | json_reformat > /tmp/i.json"
  `#{comando}`
  
  cadjson = File.read("/tmp/i.json")
  d = JSON.parse(cadjson)
  prim = true
  d.each do |r|
    if Fotoe14.where(id_externo: r["id"])
              .where(eleccion_id: Rails.configuration.x.eleccion).count == 0
      f = Fotoe14.create!(
        eleccion_id: Rails.configuration.x.eleccion,
        id_externo: r["id"],
        url: "#{Rails.configuration.x.url_base_imagenes}#{r["image"].split('/').last}",
        info: r.except("id", "image").to_json
      )
      if prim
        puts "Poblando #{r["id"]}"
        prim = false
      end
      if r["image"] =~ /^images\/[0-9]*_E14_PRE_X_(..)_(...)_(...)_XX_(..)_(...)_X_XXX.pdf$/
        print "+"
        coddep=$~[1]
        codmun=$~[2]
        zona=$~[3]
        puesto=$~[4]
        mesa=$~[5]
        dep = Sip::Departamento.where(id_pais: 170, codreg: coddep.to_i).take
        if !dep
          puts "Departamento {#coddep} no encontrado"
          next
        end
        mun = Sip::Municipio.where(id_departamento: dep.id,
                                   codreg: codmun.to_i).take
        if !mun
          puts "Municipio #{codmun} no encontrado en departamento #{dep.nombre} (#{dep.id})"
          next
        end
        zonab = Zona.where(municipio_id: mun.id, zona: zona.to_i).take
        if !zonab
          puts "Zona #{zona} no encontrada en municipio #{mun.nombre}-#{dep.nombre} (#{mun.id})"
          next
        end
        puestob = Puesto.where(zona_id: zonab.id, puesto: puesto).take
        if !puestob
          puts "Puesto #{puesto} no encontrado en zona #{zona}-#{mun.nombre}-#{dep.nombre} (#{zonab.id})"
          next
        end
        mesab = Mesa.where(puesto_id: puestob.id, mesa: mesa.to_i).take
        if !mesab
          puts "Mesa #{mesa} no encontrada en puesto #{puesto}-#{zona}-#{mun.nombre}-#{dep.nombre} (#{puestob.id})"
          next
        end
        f.mesa_id = mesab.id
        f.save!
      else
        print "."
      end
    else
      print "-"
    end
  end
  File.unlink('/tmp/i.json')
end

descarga
