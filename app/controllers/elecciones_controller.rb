class EleccionesController < ApplicationController
  before_action :set_eleccion, only: [
    :departamental, 
    :destroy,  
    :edit, 
    :mesa,
    :municipal,
    :nacional, 
    :puesto,
    :show, 
    :update,
    :zona
  ]

  def clase
    "::Eleccion"
  end

  def atributos_index
    [ 
      :id,
      :nombre,
      :numcandidatos,
      :geoelectoral_id,
      :fechaini_localizada,
      :fechafin_localizada
    ]
  end

  def atributos_show
    [ 
      :id,
      :nombre,
      :numcandidatos,
      :geoelectoral_id,
      :candidato1,
      :candidato2,
      :candidato3,
      :candidato4,
      :candidato5,
      :candidato6,
      :candidato7,
      :candidato8,
      :candidato9,
      :candidato10,
      :candidato11,
      :candidato12,
      :fechaini_localizada,
      :fechafin_localizada
    ]
  end

  def atributos_form
    atributos_show - [:id]
  end


  def vistas_manejadas
    ['Elecciones']
  end

  def nacional
    render layout: 'application'
  end

  def departamental
    poner_departamento

    render layout: 'application'
  end

  def municipal 
    poner_municipio

    render layout: 'application'
  end

  def zona 
    poner_zona

    render layout: 'application'
  end

  def puesto 
    poner_puesto

    render layout: 'application'
  end

  def mesa
    poner_mesa

    render layout: 'application'
  end


  private
    def set_eleccion
      @eleccion = @registro = Eleccion.find(params[:id])
    end

    def poner_departamento
      @departamento =  Sip::Departamento.
        where(id_pais: 170).
        where('sip_departamento.codreg=?', params[:coddep]).take!
    end

    def poner_municipio
      poner_departamento
      @municipio =  Sip::Municipio.
        where(id_departamento: @departamento.id).
        where('sip_municipio.codreg = ?', params[:codmun]).take!
    end

    def poner_zona
      poner_municipio
      @zona =  ::Zona.
        where(municipio_id: @municipio.id).
        where('zona.zona = ?', params[:zona]).take!
    end

    def poner_puesto
      poner_zona
      @puesto =  ::Puesto.
        where(zona_id: @zona.id).
        where('puesto.puesto = ?', params[:puesto]).take!
    end

    def poner_mesa
      poner_puesto
      @mesa =  ::Mesa.
        where(puesto_id: @puesto.id).
        where('mesa.mesa = ?', params[:mesa]).take!
    end


    def lista_params
      atributos_form
    end

    # Only allow a list of trusted parameters through.
    def eleccion_params
      params.require(:eleccion).permit(atributos_form)
    end
end
