class ApplicationRecord < ActiveRecord::Base
  include Sip::Modelo
  include Sip::Localizacion
  #include Sip::FormatoFecha

  self.abstract_class = true
end
