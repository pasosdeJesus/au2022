class Fotoe14 < ApplicationRecord
  require "open-uri"

  belongs_to :eleccion, optional: false, validate: true
  has_many :tabulae14
  has_many :denuncia_foto_mala

  scope :sin_tabular, -> (usuario) { where.not(id: Tabulae14.select(:fotoe14_id).where(usuario_id: usuario.id))}
  scope :sin_denuncia, -> (usuario) { where.not(id: DenunciaFotoMala.select(:fotoe14_id).where(usuario_id: usuario.id)) }
  scope :sin_multiples_denuncias, -> { left_joins(:denuncia_foto_mala)
    .group('fotoe14.id')
    .having('COUNT(denuncia_foto_mala.fotoe14_id) < 6')
    .order('COUNT(denuncia_foto_mala.fotoe14_id) ASC') }
  scope :menos_tabulado, -> { left_joins(:tabulae14)
    .group('fotoe14.id')
    .having('COUNT(tabulae14.fotoe14_id) < 6')
    .order('COUNT(tabulae14.fotoe14_id) ASC') }

  def self.para_tabular(eleccion_id, usuario)
    self.sin_tabular(usuario)
        .sin_denuncia(usuario)
        .sin_multiples_denuncias
        .menos_tabulado
        .where(eleccion_id: eleccion_id)
        .first
    # Cada imagen puede ser tabulada varias veces

    # Cada imagen puede ser invalidada por varios usuarios, debido a su ilegibilidad, 
    # incompletitud o que es otra cosa, p.e. porno o lo que sea...

    # Cuando un usuario termina, se le ofrece la imagen que tenga menos invalidaciones, 
    # que no haya sido invalidada por el usuario y tampoco haya sido tabulada por el usuario
  end

  def url_local
    ruta = Rails.root.join('public/e14pdfs')    
    archivo_local = url.split("/").last
    rutac = ruta.join(archivo_local)
    if !File.exists?(rutac)
      system("wget -P #{ruta} #{url}")
    end

    "/e14pdfs/#{archivo_local}"
  end
end
