require "test_helper"

class EleccionTest < ActiveSupport::TestCase

  test "valido" do
    eleccion = Eleccion.create PRUEBA_ELECCION
    assert eleccion.valid?
    eleccion.destroy
  end

  test "no valido" do
    eleccion = Eleccion.create PRUEBA_ELECCION
    eleccion.nombre = ''
    assert_not eleccion.valid?
    eleccion.destroy
	end

end
