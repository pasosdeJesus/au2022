require "test_helper"

class Tabulae14Test < ActiveSupport::TestCase

  test "valido" do
    tabula = Tabulae14.create PRUEBA_TABULAE14
    assert tabula.valid?
    tabula.destroy
  end

  test "no valido" do
    tabula = Tabulae14.create PRUEBA_TABULAE14
    tabula.usuario_id = nil
    assert_not tabula.valid?
    tabula.destroy
  end

  test "no permite rerpetir" do
    r = PRUEBA_TABULAE14.merge(id: nil)
    tabula = Tabulae14.create r
    assert tabula.valid?
    tabula2 = Tabulae14.create r
    assert_not tabula2.valid?
    tabula.destroy
  end


end
