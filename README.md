# sal136: Prototipo de aplicación web para comparar de manera colaborativa formularios E14 con preconteo en elecciones de Colombia 2022

[![Revisado por Hound](https://img.shields.io/badge/Reviewed_by-Hound-8E64B0.svg)](https://houndci.com) [![Estado Construcción](https://gitlab.com/pasosdeJesus/sal136/badges/main/pipeline.svg)](https://gitlab.com/pasosdeJesus/sal136/-/pipelines)[![Clima del Código](https://codeclimate.com/github/pasosdeJesus/sal136/badges/gpa.svg)](https://codeclimate.com/github/pasosdeJesus/sal136) [![Cobertura de Pruebas](https://codeclimate.com/github/pasosdeJesus/sal136/badges/coverage.svg)](https://codeclimate.com/github/pasosdeJesus/sal136) [![security](https://hakiri.io/github/pasosdeJesus/sal136/master.svg)](https://hakiri.io/github/pasosdeJesus/sal136/master)



### Desarrollo

Este sistema partió de la plantilla heb412 disponible en
https://github.com/pasosdeJesus/heb412

### Instalar en modo de desarrollo

Ejecuta `bundler` y `yarn`

Copia la plantilla `.env.plantilla` en `.env`
```
cp .env.plantilla .env
```
y edita al menos:

1. `BD_USUARIO` con el usuario de la base de datos PostgreSQL.
   En adJ crearías el usuario `sipdes` con:
   ```
   doas su - _postgresql
   createuser -U postgres -h /var/www/var/run/postgresql -s sipdes
   ```
2. `BD_CLAVE` con la clave del usuario especificado en `BD_USUARIO`. 
   Por ejemplo en adJ la podrías establecer con:
    ```
   doas su - _postgresql
   psql -U postgres -h /var/www/var/run/postgresql 
   ALTER USER sipdes WITH PASSWORD 'aqui-la-clave';
   ```
   Te recomendamos recomienda poner esa clave en el archivo `~/.pgpass` en
   una línea de la forma:
   ```
   *:*:*:sipdes:aqui-la-clave
   ```
3. `BD_DES`, `BD_PRUEBA` y `BD_PRO` con nombres de las bases de datos
   de desarrollo, prueba y producción que pertenecen al usuario especificado
   en `BD_USUARIO`.  Podrías crear la de desarrollo y pruebas y alistarlas
   (digamos que la de desarrollo fuese `sal136_des`) con:
   ```
   createdb -U sipdes -h /var/www/var/run/postgresql sal136_des
   bin/rails db:drop db:create db:setup
4. `DIRAP` con el directorio donde clonaste el repositorio de
   sal136.
5. `CONFIG_HOSTS` con la IP o el dominio con el que ingresará al sistema
    de desarrollo una vez opere e `IPDES` con la IP con la cual debe 
    correr el servidor de prueba, ambos por ejemplo pueden ser 127.0.0.1
6. `PUERTODES` con el puerto donde debe correr el servidor de prueba
   por ejemplo 3000

Tras esto lanza la aplicación de prueba con:
```
bin/corre
```

