class AgregaSegundaVuelta < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      INSERT INTO public.eleccion (id, nombre, numcandidatos, candidato1, candidato2, candidato3, candidato4, candidato5, candidato6, candidato7, candidato8, candidato9, candidato10, candidato11, candidato12, fechaini, fechafin, created_at, updated_at) VALUES (2, 'Segunda Vuelta Presidente 2022', 2, 'RODOLFO HERNANDEZ', 'GUSTAVO PETRO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-13', '2022-06-19', '2022-06-18 00:00:00', '2022-06-18 00:00:00');
    SQL
  end
  def down
    execute <<-SQL
      DELETE FROM public.eleccion WHERE id=2;
    SQL

  end
end
