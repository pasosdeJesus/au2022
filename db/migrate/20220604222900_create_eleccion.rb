class CreateEleccion < ActiveRecord::Migration[7.0]
  def change
    create_table :eleccion do |t|
      t.string :nombre, limit: 255, null: false
      t.integer :numcandidatos
      t.string :candidato1, limit: 255, null: false
      t.string :candidato2, limit: 255, null: false
      t.string :candidato3, limit: 255
      t.string :candidato4, limit: 255
      t.string :candidato5, limit: 255
      t.string :candidato6, limit: 255
      t.string :candidato7, limit: 255
      t.string :candidato8, limit: 255
      t.string :candidato9, limit: 255
      t.string :candidato10, limit: 255
      t.string :candidato11, limit: 255
      t.string :candidato12, limit: 255
      t.date :fechaini
      t.date :fechafin

      t.timestamps
    end
  end
end
