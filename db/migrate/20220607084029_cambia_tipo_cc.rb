class CambiaTipoCc < ActiveRecord::Migration[7.0]
  def up
    change_column :tabulae14, :ccjurado1, :bigint
    change_column :tabulae14, :ccjurado2, :bigint
    change_column :tabulae14, :ccjurado3, :bigint
    change_column :tabulae14, :ccjurado4, :bigint
    change_column :tabulae14, :ccjurado5, :bigint
    change_column :tabulae14, :ccjurado6, :bigint
  end
  def down
    change_column :tabulae14, :ccjurado1, :integer
    change_column :tabulae14, :ccjurado2, :integer
    change_column :tabulae14, :ccjurado3, :integer
    change_column :tabulae14, :ccjurado4, :integer
    change_column :tabulae14, :ccjurado5, :integer
    change_column :tabulae14, :ccjurado6, :integer
  end
end
