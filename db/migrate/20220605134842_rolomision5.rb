class Rolomision5 < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      ALTER TABLE usuario ALTER COLUMN rol SET DEFAULT 5;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE usuario ALTER COLUMN rol SET DEFAULT 3;
    SQL
  end
end
