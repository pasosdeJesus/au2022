class CreateGeoelectoral < ActiveRecord::Migration[7.0]
  def up
    create_table :geoelectoral do |t|
      t.string :nombre, limit: 200

      t.timestamps
    end
    execute <<-SQL
      INSERT INTO geoelectoral (id, nombre, created_at, updated_at)
        VALUES (1, 'Elecciones presidenciales 2022 en Colombia',
        '2022-06-15', '2022-06-15') ;
    SQL
  end

  def down
    drop_table :geoelectoral
  end
end
