class CorrigePuestoUnDigito < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      UPDATE puesto SET puesto='0' || puesto WHERE length(puesto)=1;
    SQL
  end
  def down
    execute <<-SQL
      UPDATE puesto SET puesto=substr(puesto, 2) 
        WHERE substr(puesto, 1, 1)='0';
    SQL
  end
end
